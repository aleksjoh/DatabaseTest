const Dao = require("./dao.js");

module.exports = class PersonDao extends Dao {
  getAll(callback) {
    super.query("select navn, alder, adresse from personDrogas", [], callback);
  }

  getOne(id, callback) {
    super.query("select navn, alder, adresse from personDrogas where id=?",
      [id],
      callback
    );
  }

  createOne(json, callback) {
    var val = [json.navn, json.adresse, json.alder];
    super.query("insert into personDrogas (navn,adresse,alder) values (?,?,?)",
      val,
      callback
    );
  }

    deleteOne(id, callback){
        super.query("DELETE FROM personDrogas WHERE id=?",
            [id],
            callback
        );
    }

    updateOne(json, id, callback){
        let navn = json.navn;
        let adresse = json.adresse;
        let alder = json.alder;

        if (json.navn.trim() === ""){
            navn = super.query("SELECT navn FROM personDrogas WHERE id=?", [id], callback);
        }
        if (json.adresse.trim() === ""){
            adresse = super.query("SELECT adresse FROM personDrogas WHERE id=?", [id], callback);
        }
        if(json.alder == null){
            alder = super.query("SELECT alder FROM personDrogas WHERE id=?", [id], callback);
        }

        let val = [navn, adresse, alder, id];
        super.query("UPDATE personDrogas SET navn=?, adresse=?, alder=? WHERE id=?",
            val,
            callback
        );
    }

};
