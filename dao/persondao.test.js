var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
var pool = mysql.createPool({
    connectionLimit: 10,
    host: "mysql.stud.iie.ntnu.no",
    user: "nilstesd",
    password: "lqqWcMzq",
    database: "nilstesd",
    debug: false,
    multipleStatements: true
});

let personDao = new PersonDao(pool);


beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

test("Deleting person from database", done => {
    function callback(status, data) {
        console.log("Test callback: status=" + status + ", data=" + JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        length = data.length;
        done();
    }

    personDao.deleteOne(2, callback);
});

test("Update person from database", done => {
    let oldName;
    let oldAge;
    let oldAdress;

    personDao.getOne(1, (status, data) => {
        oldName = data.navn;
        oldAge = data.alder;
        oldAdress = data.adresse;
    });

    personDao.updateOne({navn: "Nissen", alder: 100, adresse: "Steder i verden" }, 1, () => {});

    personDao.getOne("1", (satus, data3) => {

        if(data3.error) data3 = {navn: "", alder: 2, adresse: "asd"};

        expect(oldName).not.toEqual(data3.navn);
        expect(oldAge).not.toEqual(data3.alder);
        expect(oldAdress).not.toEqual(data3.adresse);
    });
    done();
});
